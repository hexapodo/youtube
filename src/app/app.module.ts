import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { YtPlayerModule } from 'ngx-ytplayer';


import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    YtPlayerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
