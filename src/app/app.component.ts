import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ytplayer';
  videoId = 'ScLPrOAbFKo';
  volume = 50;
  duration: any = null;

  eventChange(event) {
    console.log(event);
  }

  updateCurrentTime(event) {
    console.log(event);
  }

}
